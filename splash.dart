import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:fuel_spy/dashboard.dart';

void main() {
  runApp(const Splash());
}

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      duration: 2500,
      splash: Icons.home,
      nextScreen: const DashBoard(),
      splashTransition: SplashTransition.fadeTransition,
      backgroundColor: Colors.green,
    );
  }
}
