import 'package:flutter/material.dart';

void main() {
  runApp(const FeatureB());
}

class FeatureB extends StatelessWidget {
  const FeatureB({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature B "),
        backgroundColor: Colors.amber,
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(15),
          color: Colors.amber,
          width: 400,
          height: 80,
          child: const Text(
            "FEATURE B",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 35,
            ),
          ),
        ),
      ),
    );
  }
}
